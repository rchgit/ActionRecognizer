package ph.edu.dlsu.actionrecognizer;

import java.util.HashMap;
import java.util.Vector;

/**
 * Created by reich_canlas on 5/21/2017.
 */
class RecognitionHashMap {
    public HashMap<String, Float> getRecognitions() {
        return recognitions;
    }

    private HashMap<String, Float> recognitions;

    public RecognitionHashMap(HashMap<String, Float> recognitions) {
        this.recognitions = recognitions;
    }

    RecognitionHashMap(){
        recognitions = new HashMap<>(100);
    }

    public void addRecognition(String label, float confidence){
        recognitions.put(label,confidence);
    }

    public RecognitionHashMap(Vector<String> labels, float[] confidences){
        this.recognitions = new HashMap<>();
        for(int i = 0; i < labels.size(); i++){
            recognitions.put(labels.get(i),confidences[i]);
        }
    }
}
