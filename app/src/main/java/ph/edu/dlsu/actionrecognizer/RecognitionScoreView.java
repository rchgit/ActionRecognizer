/* Copyright 2015 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

package ph.edu.dlsu.actionrecognizer;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import java.util.*;

public class RecognitionScoreView extends View {
    private static final float TEXT_SIZE_DIP = 10;

    private final int MAX_RESULTS = 3;
    private final float RESULT_THRESHOLD = 0f; // modified to practically include all labels

    private final float textSizePx;
    private final Paint fgPaint;
    private final Paint bgPaint;
    private RecognitionHashMap resultsImg;
    private RecognitionHashMap resultsAcc;
    private RecognitionHashMap resultsGyr;
    private RecognitionHashMap resultsMag;
    private RecognitionHashMap resultsFusion;


    public RecognitionScoreView(final Context context, final AttributeSet set) {
        super(context, set);

        textSizePx =
                TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP, TEXT_SIZE_DIP, getResources().getDisplayMetrics());
        fgPaint = new Paint();
        fgPaint.setTextSize(textSizePx);

        bgPaint = new Paint();
        bgPaint.setColor(0xcc4285f4);
    }

    /**
     * Sets the image result to be displayed in the View.
     *
     * @param results The recognition list for image.
     */


    /**
     * Sets the sensor result to be displayed in the View.
     * NOTE: Sensor result here refers to average of the sensor results
     *
     * @param results The recognition list for sensor averages.
     */
    void setResultsAcc(final RecognitionHashMap results) {
        this.resultsAcc = results;

    }

    void setResultsGyr(final RecognitionHashMap results) {
        this.resultsGyr = results;
//        List<Map.Entry<String,Float>> set = new LinkedList<>(resultsGyr.getRecognitions().entrySet()) ;
//        Collections.sort(set, new Comparator<Map.Entry<String, Float>>()
//        {
//            public int compare( Map.Entry<String, Float> o1, Map.Entry<String, Float> o2 )
//            {
//                return (o1.getValue()).compareTo( o2.getValue() );
//            }
//        });
    }

    void setResultsMag(final RecognitionHashMap results) {
        this.resultsMag = results;
    }
    void setResultsImg(final RecognitionHashMap results) {
        this.resultsImg = results;
    }

    /**
     * Sets the fusion result to be displayed in the View.
     *
     * @param results The recognition list for fusion.
     */

    void setResultsFusion(final RecognitionHashMap results) {
        this.resultsFusion = results;
    }

    @Override
    public void onDraw(final Canvas canvas) {
        int x = 10;
        int y = (int) (fgPaint.getTextSize() * 1.5f);
        int xIncrement = 140;
        float yScale = 1.5f;
        canvas.drawPaint(bgPaint);

        if (resultsImg != null && !resultsImg.getRecognitions().isEmpty()) {
            canvas.drawText("Img", x, y, fgPaint);
            y += fgPaint.getTextSize() * yScale;
            for (final Map.Entry<String, Float> recog : resultsImg.getRecognitions().entrySet()) {
                canvas.drawText(recog.getKey() + ": " + String.format(Locale.US, "%.3f",
                        recog.getValue()), x, y, fgPaint);
                y += fgPaint.getTextSize() * yScale;
            }
        }
        x += xIncrement;
        y = (int) (fgPaint.getTextSize() * yScale);
        if (resultsAcc != null && !resultsAcc.getRecognitions().isEmpty()) {
            canvas.drawText("Acc", x, y, fgPaint);
            y += fgPaint.getTextSize() * yScale;
            for (final Map.Entry<String, Float> recog : resultsAcc.getRecognitions().entrySet()) {
                canvas.drawText(recog.getKey() + ": " + String.format(Locale.US, "%.3f",
                        recog.getValue()), x, y, fgPaint);
                y += fgPaint.getTextSize() * yScale;
            }
        }
        x += xIncrement;
        y = (int) (fgPaint.getTextSize() * yScale);
        if (resultsGyr != null && !resultsGyr.getRecognitions().isEmpty()) {
            canvas.drawText("Gyr", x, y, fgPaint);
            y += fgPaint.getTextSize() * yScale;

            for (final Map.Entry<String, Float> recog : resultsGyr.getRecognitions().entrySet()) {
                canvas.drawText(recog.getKey() + ": " + String.format(Locale.US, "%.3f",
                        recog.getValue()), x, y, fgPaint);
                y += fgPaint.getTextSize() * yScale;
            }
        }
        x += xIncrement;
        y = (int) (fgPaint.getTextSize() * yScale);
        if (resultsMag != null && !resultsMag.getRecognitions().isEmpty()) {
            canvas.drawText("Mag", x, y, fgPaint);
            y += fgPaint.getTextSize() * yScale;
            for (final Map.Entry<String, Float> recog : resultsMag.getRecognitions().entrySet()) {
                canvas.drawText(recog.getKey() + ": " + String.format(Locale.US, "%.3f",
                        recog.getValue()), x, y, fgPaint);
                y += fgPaint.getTextSize() * yScale;
            }
        }
        x += xIncrement;
        y = (int) (fgPaint.getTextSize() * yScale);
        if (resultsFusion != null && !resultsFusion.getRecognitions().isEmpty()) {
            canvas.drawText("Fusion", x, y, fgPaint);
            y += fgPaint.getTextSize() * yScale;
            for (final Map.Entry<String, Float> recog : resultsFusion.getRecognitions().entrySet()) {
                canvas.drawText(recog.getKey() + ": " + String.format(Locale.US, "%.3f",
                        recog.getValue()), x, y, fgPaint);
                y += fgPaint.getTextSize() * yScale;
            }
        }

        

    }

}
