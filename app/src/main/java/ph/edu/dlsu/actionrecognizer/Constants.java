package ph.edu.dlsu.actionrecognizer;

/**
 * Created by reich_canlas on 2/21/17.
 */
class Constants {

    static final float TEXT_SIZE_DIP = 10;
    static final int NUM_THREADS = 20;
    static final int IMG_FEATURE_SIZE = 1024;
    static final int NUM_CLASSES = 9;
    static final int IMAGE_INPUT_SIZE = 224;
    static final int IMAGE_MEAN = 117;
    static final int NUM_CHANNELS_IMG = 3;
    static final float IMAGE_STD = 1;
    static final int NUM_TIME_STEPS_IMG = 30;
    static final int NUM_TIME_STEPS_ACC = 120;
    static final int NUM_TIME_STEPS_GYR = 120;
    static final int NUM_TIME_STEPS_MAG = 60;
    static final int NUM_SENSORS = 3; // 0 = accel, 1 = gyro, 2 = magnetic
    private static final int NUM_AXIS = 3; //  0 = x, 1 = y, 2 = z
    static final int NUM_CHANNELS_SENSOR = NUM_AXIS;


    static final String ACC_INPUT = "acc/input_acc";
    static final String ACC_OUTPUT = "acc/pred_acc/Softmax";
    static final String FUSION_AVE_IMG_INPUT_NAME = "Fusion/img_recog";
    static final String FUSION_AVE_ACC_INPUT_NAME = "Fusion/acc_recog";
    static final String FUSION_AVE_GYR_INPUT_NAME = "Fusion/gyr_recog";
    static final String FUSION_AVE_MAG_INPUT_NAME = "Fusion/mag_recog";
    static final String FUSION_AVE_OUTPUT_NAME = "Fusion/fusion_ave";
    static final String INCEPTION_INPUT_NAME = "InceptionGraph/div";
    static final String INCEPTION_OUTPUT_NAME = "InceptionGraph/InceptionV1/Logits/MaxPool_0a_7x7/AvgPool";
    static final String FUSION_IMG_INPUT_NAME = "Fusion/img_recog";
    static final String FUSION_ACC_INPUT_NAME = "Fusion/acc_recog";
    static final String FUSION_GYR_INPUT_NAME = "Fusion/gyr_recog";
    static final String FUSION_MAG_INPUT_NAME = "Fusion/mag_recog";
    static final String FUSION_OUTPUT_NAME = "pred_fusion/Softmax";
    static final String IMG_LSTM_INPUT = "img/input_img";
    static final String IMG_LSTM_OUTPUT = "img/pred_img/Softmax";
    static final String MAG_INPUT = "mag/input_mag";
    static final String MAG_OUTPUT = "mag/pred_mag/Softmax";
    static final long[] SHAPE_SENSOR_ACC = new long[]{1, NUM_TIME_STEPS_ACC, NUM_CHANNELS_SENSOR};
    static final long[] SHAPE_SENSOR_GYR = new long[]{1, NUM_TIME_STEPS_GYR, NUM_CHANNELS_SENSOR};
    static final long[] SHAPE_SENSOR_MAG = new long[]{1, NUM_TIME_STEPS_MAG, NUM_CHANNELS_SENSOR};
    static final String GYR_INPUT = "gyr/input_gyr";
    static final String GYR_OUTPUT = "gyr/pred_gyr/Softmax";
    static final String MEAN_INPUT_NAME = "Mean/mean_input";
    static final String MEAN_OUTPUT_NAME = "Mean/mean_output";

    static final float[] SENSOR_MEANS = new float[]{0.20966922680000002f, 9.1002750524000007f, -0.626904854f,
            0.00582687378998f, 0.000904789752089f, -0.00594663733333f,
            -0.436911099333f, -12.3393455376f, -6.04097745253f};
    static final float[] ACC_MEANS = new float[]{0.20966922680000002f, 9.1002750524000007f, -0.626904854f};
    static final float[] GYR_MEANS = new float[]{0.00582687378998f, 0.000904789752089f, -0.00594663733333f};
    static final float[] MAG_MEANS = new float[]{-0.436911099333f, -12.3393455376f, -6.04097745253f};
    static final float[] SENSOR_STDS = new float[]{1.334403717866667f, 3.59645970067f,
            3.32057264307f,
            1.11780491213f,
            0.873958978667f,
            0.442064657333f,
            25.0976113129f,
            10.5407813264f,
            36.4405282591f};
    static final float[] ACC_STDS = new float[]{1.334403717866667f, 3.59645970067f,   3.32057264307f};
    static final float[] GYR_STDS = new float[]{1.11780491213f, 0.873958978667f, 0.442064657333f};
    static final float[] MAG_STDS = new float[]{25.0976113129f,10.5407813264f,36.4405282591f};

}
