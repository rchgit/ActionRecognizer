/*
 * Copyright 2016 The TensorFlow Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ph.edu.dlsu.actionrecognizer;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.Image;
import android.media.Image.Plane;
import android.media.ImageReader;
import android.media.ImageReader.OnImageAvailableListener;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.util.Size;
import android.util.TypedValue;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import com.opencsv.CSVWriter;
import ph.edu.dlsu.actionrecognizer.env.BorderedText;
import ph.edu.dlsu.actionrecognizer.env.ImageUtils;
import ph.edu.dlsu.actionrecognizer.env.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static ph.edu.dlsu.actionrecognizer.Constants.*;

@SuppressWarnings("unchecked")
public class CameraActivity extends Activity implements OnImageAvailableListener,
        SensorEventListener, CameraConnectionFragment.ConnectionCallback {
   private static final Logger LOGGER = new Logger();

   private static final int PERMISSIONS_REQUEST = 1;
   private static final String PERMISSION_CAMERA = Manifest.permission.CAMERA;
   private static final String PERMISSION_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
   private static final String TAG = "CameraActivity";
   private static final String FUSION_MODEL_FILE = "file:///android_asset/har.pb";
   private static final String LABEL_FILE =
           "file:///android_asset/har_labels.txt";
   private static final boolean SAVE_PREVIEW_BITMAP = false;
   private static final boolean MAINTAIN_ASPECT = true;
   private Random rand = new Random();

   static {
      System.loadLibrary("imageutils_jni");
   }

   private boolean debug = false;
   private int previewWidth = 0;
   private int previewHeight = 0;
   private byte[][] yuvBytesFrame;
   private int[] rgbBytesFrame = null;
   private Bitmap rgbFrameBitmap = null;
   private Bitmap croppedBitmapFrame = null;
   private boolean computingImage = false;
   private Matrix frameToCropTransform;
   private Matrix cropToFrameTransform;
   private RecognitionScoreView resultsView;
   private BorderedText borderedText;
   private final long inferenceTimeStart = System.currentTimeMillis();
   private long inferenceTimeImg = inferenceTimeStart;
   private long inferenceTimeAcc = inferenceTimeStart;
   private long inferenceTimeGyr = inferenceTimeStart;
   private long inferenceTimeMag = inferenceTimeStart;
   private long inferenceTimeFusion = inferenceTimeStart;
   private StringBuilder log = new StringBuilder();
   private boolean inferenceActive = true;
   private float[] accFlatQueue;
   private float[] gyrFlatQueue;
   private float[] magFlatQueue;
   private ArrayList<int[]> inceptionQueueInt = new ArrayList<>(NUM_TIME_STEPS_IMG);

   public boolean isInferenceActive() {
      return inferenceActive;
   }

   public void setInferenceActive(boolean inferenceActive) {
      this.inferenceActive = inferenceActive;
   }

   /**
    * The sensor manager.
    */
   private SensorManager mSenseManager;
   /**
    * The sensor objects.
    */
   private Sensor mAcc, mGyr, mMag;
   private String mRootPath, mResultsPath, mDataPath, mLogCatPath;
   private CSVWriter mAccDataFile, mGyrDataFile, mMagDataFile, mAccResultsFile, mGyrResultsFile, mMagResultsFile, mImgResultsFile;
   private FileWriter logcatWriter;
   private int fusionRecogCount;
   private TFFusionClassifier classifier;
   private ArrayList<float[]> accQueue = new ArrayList<>(NUM_TIME_STEPS_ACC);
   private ArrayList<float[]> gyrQueue = new ArrayList<>(NUM_TIME_STEPS_GYR);
   private ArrayList<float[]> magQueue = new ArrayList<>(NUM_TIME_STEPS_MAG);
   private ArrayList<float[]> inceptionQueue = new ArrayList<>(NUM_TIME_STEPS_IMG);
   private float[] imgRecog = new float[NUM_CLASSES];
   private PowerManager pm;
   private PowerManager.WakeLock wl;

   private final ExecutorService executor = Executors.newFixedThreadPool(NUM_THREADS);

   private int accRecogCount = 0;
   private int gyrRecogCount = 0;
   private int magRecogCount = 0;
   private float[] accRecog = new float[NUM_CLASSES];
   private float[] gyrRecog = new float[NUM_CLASSES];
   private float[] magRecog = new float[NUM_CLASSES];
   private int imgRecogCount;

   private static boolean allTrue(boolean[] values) {
      for (boolean value : values) {
         if (!value)
            return false;
      }
      return true;
   }

   private synchronized void setComputingImage(boolean computingImage) {
      this.computingImage = computingImage;
   }

   private String getDataFilePath(Context context) {
      String path;
      try {
         path = context.getExternalFilesDir(null).getAbsolutePath() + "/";
      } catch (final NullPointerException e) {
         Log.d(context.getPackageName(), "File can't be written");
         path = "";
      }
      return path;
   }

   @Override
   protected void onCreate(final Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
//        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
      requestWindowFeature(Window.FEATURE_NO_TITLE);
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
      setContentView(R.layout.activity_camera);
      pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
      wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "ClassifierWake");
      wl.acquire();


      if (hasPermission()) {
         setFragment();

      } else requestPermission();
   }

   @Override
   public void onStart() {
      // As suggested in StackOverflow
      // try moving non-essential code (i.e not setContentView or shared prefs)
      // in onCreate() to onStart() instead and see what happens
      LOGGER.d("onStart " + this);
      super.onStart();
      // Make folders as necessary
      mRootPath = getDataFilePath(this);

      File dataDir = new File(mRootPath, "data");
      File resultsDir = new File(mRootPath, "results");
      File logCatDir = new File(mRootPath, "logcat");

      if (!dataDir.exists() && dataDir.mkdir()) Log.i(TAG, "dataDir created");
      if (!resultsDir.exists() && resultsDir.mkdir()) Log.i(TAG, "resultsDir created");
      if (!logCatDir.exists() && logCatDir.mkdir()) Log.i(TAG, "logCatDir created");

      mDataPath = mRootPath + "data/";
      mResultsPath = mRootPath + "results/";
      mLogCatPath = mRootPath + "logcat/";


      // Initialize TensorFlow inference engines
      try {
         classifier = new TFFusionClassifier(getAssets(), FUSION_MODEL_FILE, LABEL_FILE);
         Log.d(TAG, "TensorFlow init done.");
      } catch (final IOException e) {
         throw new RuntimeException("IO Exception: Error initializing TFFusionClassifier!", e);
      }

      // CSV Files
      try {
         mAccDataFile = new CSVWriter(new FileWriter(mDataPath + "data-" + inferenceTimeStart +
                 "-acc.csv"), ',', CSVWriter.NO_QUOTE_CHARACTER);
         mGyrDataFile = new CSVWriter(new FileWriter(mDataPath + "data-" + inferenceTimeStart +
                 "-gyr.csv"), ',', CSVWriter.NO_QUOTE_CHARACTER);
         mMagDataFile = new CSVWriter(new FileWriter(mDataPath + "data-" + inferenceTimeStart +
                 "-mag.csv"), ',', CSVWriter.NO_QUOTE_CHARACTER);
         mAccResultsFile = new CSVWriter(new FileWriter(mResultsPath + "results-" + inferenceTimeStart +
                 "-acc.csv"), ',', CSVWriter.NO_QUOTE_CHARACTER);
         mGyrResultsFile = new CSVWriter(new FileWriter(mResultsPath + "results-" + inferenceTimeStart +
                 "-gyr.csv"), ',', CSVWriter.NO_QUOTE_CHARACTER);
         mMagResultsFile = new CSVWriter(new FileWriter(mResultsPath + "results-" + inferenceTimeStart +
                 "-mag.csv"), ',', CSVWriter.NO_QUOTE_CHARACTER);
         mImgResultsFile = new CSVWriter(new FileWriter(mResultsPath + "results-" + inferenceTimeStart +
                 "-img.csv"), ',', CSVWriter.NO_QUOTE_CHARACTER);
         Vector<String> headers = (Vector) classifier.getLabels().clone();
         headers.add(0, "Timestamp");
         String[] headerString = new String[NUM_CLASSES + 1];
         headers.toArray(headerString);
         mAccResultsFile.writeNext(headerString);
         mGyrResultsFile.writeNext(headerString);
         mMagResultsFile.writeNext(headerString);
         mImgResultsFile.writeNext(headerString);


      } catch (final IOException e) {
         Log.e(TAG, e.getMessage());
      }

      // Initialize sensors
      mSenseManager = (SensorManager) getSystemService(SENSOR_SERVICE);
      mAcc = mSenseManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
      mGyr = mSenseManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
      mMag = mSenseManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
      Log.d(TAG, "Sensor init done.");
      mSenseManager.registerListener(CameraActivity.this, mAcc,
              SensorManager.SENSOR_DELAY_GAME, NUM_TIME_STEPS_ACC * 20000);
      mSenseManager.registerListener(CameraActivity.this, mGyr,
              SensorManager.SENSOR_DELAY_GAME, NUM_TIME_STEPS_GYR * 20000);
      mSenseManager.registerListener(CameraActivity.this, mMag,
              SensorManager.SENSOR_DELAY_GAME, NUM_TIME_STEPS_MAG * 20000);

   }

   @Override
   public synchronized void onResume() {
      LOGGER.d("CameraActivity", "onResume " + this);
      super.onResume();
   }

   @Override
   public void onPause() {
      LOGGER.d("onPause " + this);
      super.onPause();
      try {
         logcatWriter = new FileWriter(mLogCatPath + "logcat-" + inferenceTimeStart + ".txt", true);
         logcatWriter.write(log.toString());
         logcatWriter.close();
      } catch (final IOException e) {
         Log.e(TAG, e.getMessage());
      }
      try {
         mAccDataFile.flush();
         mGyrDataFile.flush();
         mMagDataFile.flush();
         mAccDataFile.close();
         mGyrDataFile.close();
         mMagDataFile.close();
         mAccResultsFile.flush();
         mGyrResultsFile.flush();
         mMagResultsFile.flush();
         mImgResultsFile.flush();
         mAccResultsFile.close();
         mGyrResultsFile.close();
         mMagResultsFile.close();
         mImgResultsFile.close();

      } catch (final IOException e) {
         Log.e(TAG, "CSV File already closed.");
      } catch (final NullPointerException e) {
         Log.e(TAG, "No CSV file created, so nothing to close.");
      }
   }

   @Override
   public void onStop() {
      LOGGER.d("onStop " + this);
      executor.shutdown();
      try {
         mSenseManager.unregisterListener(this);
         if (classifier != null)
            classifier.close();
         if (wl != null)
            wl.release();
      } catch (final Exception e) {
         LOGGER.e(e.getMessage());
      }
      super.onStop();

   }

   @Override
   public void onDestroy() {
      LOGGER.d("onDestroy " + this);
      super.onDestroy();
   }

   @Override
   public void onRequestPermissionsResult(
           final int requestCode, final String[] permissions, final int[] grantResults) {
      switch (requestCode) {
         case PERMISSIONS_REQUEST: {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
               setFragment();
            } else {
               requestPermission();
            }
         }
      }
   }

   private boolean hasPermission() {
      return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
              checkSelfPermission(PERMISSION_CAMERA) ==
                      PackageManager.PERMISSION_GRANTED &&
                      checkSelfPermission(PERMISSION_STORAGE) == PackageManager.PERMISSION_GRANTED;
   }

   private void requestPermission() {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
         if (shouldShowRequestPermissionRationale(PERMISSION_CAMERA) || shouldShowRequestPermissionRationale(PERMISSION_STORAGE)) {
            Toast.makeText(CameraActivity.this, "Camera AND storage permission are required for this demo",
                    Toast.LENGTH_LONG).show();
         }
         requestPermissions(new String[]{PERMISSION_CAMERA, PERMISSION_STORAGE}, PERMISSIONS_REQUEST);
      }
   }

   private void setFragment() {
      final Fragment fragment = new CameraConnectionFragment();
      getFragmentManager()
              .beginTransaction()
              .replace(R.id.container, fragment)
              .commit();
   }

   private void fillBytes(final Plane[] planes, final byte[][] yuvBytes) {
      // Because of the variable row stride it's not possible to know in
      // advance the actual necessary dimensions of the yuv planes.
      for (int i = 0; i < planes.length; ++i) {
         final ByteBuffer buffer = planes[i].getBuffer();
         if (yuvBytes[i] == null) {
            LOGGER.d("Initializing buffer %d at size %d", i, buffer.capacity());
            yuvBytes[i] = new byte[buffer.capacity()];
         }
         buffer.get(yuvBytes[i]);
      }
   }

   public boolean isDebug() {
      return debug;
   }

   private void requestRender() {
      final OverlayView overlay = (OverlayView) findViewById(R.id.debug_overlay);
      if (overlay != null) {
         overlay.postInvalidate();
      }
   }

   private void addCallback(final OverlayView.DrawCallback callback) {
      final OverlayView overlay = (OverlayView) findViewById(R.id.debug_overlay);
      if (overlay != null) {
         overlay.addCallback(callback);
      }
   }


   public void onPreviewSizeChosen(final Size size, final int rotation) {
      final float textSizePx =
              TypedValue.applyDimension(
                      TypedValue.COMPLEX_UNIT_DIP, TEXT_SIZE_DIP, getResources().getDisplayMetrics());
      borderedText = new BorderedText(textSizePx);
      borderedText.setTypeface(Typeface.SANS_SERIF);

      previewWidth = size.getWidth();
      previewHeight = size.getHeight();

      final Display display = getWindowManager().getDefaultDisplay();
      final int screenOrientation = display.getRotation();

      LOGGER.i("Sensor orientation: %d, Screen orientation: %d", rotation, screenOrientation);

      Integer sensorOrientation = rotation + screenOrientation;

      LOGGER.i("Initializing at size %dx%d", previewWidth, previewHeight);
      rgbBytesFrame = new int[previewWidth * previewHeight];
      rgbFrameBitmap = Bitmap.createBitmap(previewWidth, previewHeight, Bitmap.Config.ARGB_8888);
      croppedBitmapFrame = Bitmap.createBitmap(IMAGE_INPUT_SIZE, IMAGE_INPUT_SIZE, Bitmap.Config.ARGB_8888);

      frameToCropTransform =
              ImageUtils.getTransformationMatrix(
                      previewWidth, previewHeight,
                      IMAGE_INPUT_SIZE, IMAGE_INPUT_SIZE,
                      sensorOrientation, MAINTAIN_ASPECT);

      cropToFrameTransform = new Matrix();
      frameToCropTransform.invert(cropToFrameTransform);

      yuvBytesFrame = new byte[3][];
      resultsView = (RecognitionScoreView) findViewById(R.id.results);

   }

   int getLayoutId() {
      return R.layout.camera_connection_fragment;
   }

   int getDesiredPreviewFrameSize() {
      return IMAGE_INPUT_SIZE;
   }

   /**
    * Event handler when an image is available from the camera.
    * NOTE: This runs on a separate thread
    *
    * @param reader An ImageReader object attached to the camera.
    */

   @Override
   public synchronized void onImageAvailable(final ImageReader reader) {
//        Log.d(TAG, "Image available");
//        if (!inferenceActive) return;
      if (computingImage) return;

      if (inceptionQueueInt.size() < NUM_TIME_STEPS_IMG) {
         try (Image image = reader.acquireLatestImage()) {
            if (image == null) {
               return;
            }

            // Extract YUV data from camera and convert to ARGB8888
            final Plane[] planesFrame = image.getPlanes();
            fillBytes(planesFrame, yuvBytesFrame);

            final int yRowStrideFrame = planesFrame[0].getRowStride();
            final int uvRowStrideFrame = planesFrame[1].getRowStride();
            final int uvPixelStrideFrame = planesFrame[1].getPixelStride();

            ImageUtils.convertYUV420ToARGB8888(
                    yuvBytesFrame[0],
                    yuvBytesFrame[1],
                    yuvBytesFrame[2],
                    rgbBytesFrame,
                    previewWidth,
                    previewHeight,
                    yRowStrideFrame,
                    uvRowStrideFrame,
                    uvPixelStrideFrame,
                    false);

         } catch (final Exception e) {
            LOGGER.e(e, "Exception!");
            return;
         }
         rgbFrameBitmap.setPixels(rgbBytesFrame, 0, previewWidth, 0, 0, previewWidth, previewHeight);
         final Canvas canvasFrame = new Canvas(croppedBitmapFrame);
         canvasFrame.drawBitmap(rgbFrameBitmap, frameToCropTransform, null);
         int[] imageIntValues = new int[IMAGE_INPUT_SIZE * IMAGE_INPUT_SIZE];
         // For examining the actual TF input.
//            imgRecogCount++;
         if (SAVE_PREVIEW_BITMAP) {
            executor.execute(new Runnable() {
               @Override
               public void run() {
                  ImageUtils.saveBitmap(croppedBitmapFrame, mDataPath, inferenceTimeStart +
                          "-img-" + imgRecogCount + "-frame-" + inceptionQueue.size() + ".png");
               }
            });

         }
         croppedBitmapFrame.getPixels(imageIntValues, 0, croppedBitmapFrame.getWidth(),
                 0, 0, croppedBitmapFrame.getWidth(), croppedBitmapFrame.getHeight());
//            final float[] imageFloatValues = TFFusionClassifier.standardizeImage(imageIntValues);
         inceptionQueueInt.add(imageIntValues);


      } else {
         // This is for purging the image buffers
         Image image = reader.acquireLatestImage();
         image.close();

         // Make sure that no new sensor data is being acquired before taking
         // the final average

         setComputingImage(true);


//            writeRecogToCSV(imgRecog, mImgResultsFile);
         executor.execute(new Runnable() {
            @Override
            public void run() {

               for (int[] anInceptionQueueInt : inceptionQueueInt) {
                  inceptionQueue.add(classifier.recognizeInception
                          (TFFusionClassifier.standardizeImage(anInceptionQueueInt)));
               }
               inceptionQueueInt.clear();
               float[] imgFlatQueue = TFFusionClassifier.flattenSensor
                       (inceptionQueue, NUM_TIME_STEPS_IMG, IMG_FEATURE_SIZE);



               final float[] fusion = classifier.recogFusion(imgFlatQueue,
                       accFlatQueue, gyrFlatQueue, magFlatQueue);

               imgRecog = classifier.recognizeImg(imgFlatQueue);
               accRecog = classifier.recognizeAcc(accFlatQueue);
               gyrRecog = classifier.recognizeGyr(gyrFlatQueue);
               magRecog = classifier.recognizeMag(magFlatQueue);


               CameraActivity.this.runOnUiThread(new Runnable() {
                  @Override
                  public void run() {

                     accQueue.clear();
                     gyrQueue.clear();
                     magQueue.clear();
                     inceptionQueue.clear();
                     inceptionQueueInt.clear();
                     setComputingImage(false);
                     RecognitionHashMap fusionHash, accHash, gyrHash, magHash, imgHash;
                     accHash = new RecognitionHashMap(classifier.getLabels(), accRecog);
                     gyrHash = new RecognitionHashMap(classifier.getLabels(), gyrRecog);
                     magHash = new RecognitionHashMap(classifier.getLabels(), magRecog);
                     imgHash = new RecognitionHashMap(classifier.getLabels(), imgRecog);
                     fusionHash = new RecognitionHashMap(classifier.getLabels(), fusion);
                     updateScoreView(imgHash, accHash, gyrHash, magHash, fusionHash);

                  }
               });
            }
         });

      }

   }

   /**
    * Sensor event listener when new values are acquired.
    *
    * @param event The sensor change event.
    */

   @Override
   public synchronized void onSensorChanged(SensorEvent event) {
      long infPeriod;
      if (computingImage) return;
      switch (event.sensor.getType()) {
         case Sensor.TYPE_ACCELEROMETER:
            if (accQueue.size() < NUM_TIME_STEPS_ACC) {
               accQueue.add(classifier.standardizeSensor(event.values,
                       ACC_MEANS, ACC_STDS));

            } else {
               executor.execute(new Runnable() {
                  @Override
                  public void run() {
                     accFlatQueue = TFFusionClassifier.flattenSensor(accQueue, NUM_TIME_STEPS_ACC);
                  }
               });

//                    accQueue.clear();
            }
            break;

         case Sensor.TYPE_GYROSCOPE:
            if (gyrQueue.size() < NUM_TIME_STEPS_GYR) {
               gyrQueue.add(classifier.standardizeSensor(event.values,
                       GYR_MEANS, GYR_STDS));
            } else {
               executor.execute(new Runnable() {
                  @Override
                  public void run() {
                     gyrFlatQueue = TFFusionClassifier.flattenSensor(gyrQueue, NUM_TIME_STEPS_GYR);
                  }
               });

//                    gyrQueue.clear();

            }
            break;

         case Sensor.TYPE_MAGNETIC_FIELD:

            if (magQueue.size() < NUM_TIME_STEPS_MAG) {
               magQueue.add(classifier.standardizeSensor(event.values,
                       MAG_MEANS, MAG_STDS));
            } else {
               executor.execute(new Runnable() {
                  @Override
                  public void run() {
                     magFlatQueue = TFFusionClassifier.flattenSensor(magQueue, NUM_TIME_STEPS_MAG);
                  }
               });

//                    magQueue.clear();

            }
            break;
      }
   }

   private void updateScoreView(RecognitionHashMap imgHash, RecognitionHashMap accHash,
                                RecognitionHashMap gyrHash, RecognitionHashMap magHash,
                                RecognitionHashMap fusionHash) {
      resultsView.setResultsImg(imgHash);
      resultsView.setResultsAcc(accHash);
      resultsView.setResultsGyr(gyrHash);
      resultsView.setResultsMag(magHash);
      resultsView.setResultsFusion(fusionHash);
      resultsView.invalidate();
   }

   private void writeQueueToCSV(ArrayList<float[]> queue, CSVWriter file) {
      StringBuilder buf = new StringBuilder();
      for (float[] arr : queue) {
         for (float a : arr) {
            buf.append(a);
            buf.append(',');
         }
         String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH:mm:ss:SSSSS", Locale.US)
                 .format(new Date());
         String sb = timeStamp + ',' + buf.toString();
         String[] sb_split = sb.split(",");
         file.writeNext(sb_split);
         buf = new StringBuilder();
      }
   }

   private void writeRecogToCSV(float[] recog, CSVWriter file) {
      StringBuilder buf = new StringBuilder();

      for (float a : recog) {
         buf.append(a);
         buf.append(',');
      }
      String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH:mm:ss:SSSSS", Locale.US)
              .format(new Date());
      String sb = timeStamp + ',' + buf.toString();
      String[] sb_split = sb.split(",");
      file.writeNext(sb_split);

   }


   @Override
   public void onAccuracyChanged(Sensor sensor, int i) {
      // EMPTY. THIS JUST HAD TO BE IMPLEMENTED FOR AN INTERFACE.
   }


}
