package ph.edu.dlsu.actionrecognizer;

import android.content.res.AssetManager;
import android.util.Log;
import org.tensorflow.Graph;
import org.tensorflow.Session;
import org.tensorflow.Tensor;
import org.tensorflow.TensorFlow;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Vector;

import static ph.edu.dlsu.actionrecognizer.Constants.*;

/**
 * Created by reich_canlas on 4/22/2017.
 */
class TFFusionClassifier {

    private static final String TAG = "FusionClassifier";
    // Only return this many results with at least this confidence.

    static {
        System.loadLibrary("tensorflow_inference");
    }

    // Pre-allocated buffers.
    private Vector<String> labels = new Vector<>();
    private Session sess;

    TFFusionClassifier(final AssetManager assetManager, final String modelFilename,
                       final String labelFilename) throws IOException {

        // Read the label names into memory.
        Log.i(TAG, "TensorFlow version: " + TensorFlow.version());
//        Log.i(TAG, "TensorFlow available ops: " + Arrays.toString(TensorFlow.registeredOpList()));
        String actualFilename = labelFilename.split("file:///android_asset/")[1];
        String actualFilenameGraph = modelFilename.split("file:///android_asset/")[1];

        Log.i(TAG, "Reading labels from: " + actualFilename);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(assetManager.open(actualFilename)))) {
            String line;
            while ((line = br.readLine()) != null) {
                this.labels.add(line);
            }
            Log.i(TAG, "Read " + this.labels.size() + " labels, " + NUM_CLASSES +
                    " output layer size specified");
        }

        try (BufferedInputStream graphDefReader = new BufferedInputStream(assetManager.open(actualFilenameGraph))) {
            byte[] graphDef = new byte[graphDefReader.available()];
            graphDefReader.read(graphDef);
            Graph graph = new Graph();
            graph.importGraphDef(graphDef);
            this.sess = new Session(graph);
            Log.d("TFGraphDef", graph.toString());

        }
    }

    static float[] standardizeImage(int[] imageIntValues) {
        // Format is RGB-RGB-RGB row major
        float[] imageFloatValues = new float[IMAGE_INPUT_SIZE * IMAGE_INPUT_SIZE * 3];
        for (int i = 0; i < imageIntValues.length; ++i) {
            final int val = imageIntValues[i];
            imageFloatValues[i * 3] = (((val >> 16) & 0xFF) - IMAGE_MEAN) / IMAGE_STD;
            imageFloatValues[i * 3 + 1] = (((val >> 8) & 0xFF) - IMAGE_MEAN) / IMAGE_STD;
            imageFloatValues[i * 3 + 2] = ((val & 0xFF) - IMAGE_MEAN) / IMAGE_STD;
        }
        return imageFloatValues;
    }

    static float[] standardizeImage(byte[] imageByteValues) {
        // Format is RGB-RGB-RGB row major
        float[] imageFloatValues = new float[IMAGE_INPUT_SIZE * IMAGE_INPUT_SIZE * 3];
        for (int i = 0; i < imageByteValues.length; i++) {
            imageFloatValues[i] = ((imageByteValues[i] & 0xff) - IMAGE_MEAN) / IMAGE_STD;
        }
        return imageFloatValues;
    }

    private static float[] tensorToFloatArray(Tensor tensor) {
        FloatBuffer fb = FloatBuffer.allocate(tensor.numElements());
        tensor.writeTo(fb);
        float[] fbarray = new float[NUM_CLASSES];
        if (fb.hasArray())
            fbarray = fb.array();
        return fbarray;
    }

    static float[] flattenSensor(ArrayList<float[]> sequence, int NUM_TIME_STEPS_SENSOR) {
        float[] flat = new float[NUM_SENSORS * NUM_TIME_STEPS_SENSOR];
        for (int i = 0; i < sequence.size(); i++) {
            System.arraycopy(sequence.get(i), 0, flat, i * NUM_SENSORS, NUM_SENSORS);
        }
        return flat;
    }

    static float[] flattenSensor(ArrayList<float[]> sequence, int NUM_TIME_STEPS_SENSOR, int SIZE_FEATURES) {
        float[] flat = new float[SIZE_FEATURES * NUM_TIME_STEPS_SENSOR];
        for (int i = 0; i < sequence.size(); i++) {
            System.arraycopy(sequence.get(i), 0, flat, i * SIZE_FEATURES, SIZE_FEATURES);
        }
        return flat;
    }

    static float[] flattenRecog(ArrayList<float[]> sequence) {
        float[] flat = new float[sequence.size() * NUM_CLASSES];
        for (int i = 0; i < sequence.size(); i++) {
            System.arraycopy(sequence.get(i), 0, flat, i * NUM_CLASSES, NUM_CLASSES);
        }
        return flat;
    }

    Vector<String> getLabels() {
        return labels;
    }

    private float[] recognize(final float[] inputData, final long[] shape,
                              final String inputTensorName, final String outputTensorName) {

        try (Tensor inputTensor = Tensor.create(shape, FloatBuffer.wrap(inputData));
             Tensor output = sess.runner()
                     .feed(inputTensorName, inputTensor)
                     .fetch(outputTensorName)
                     .run().get(0)) {
            return tensorToFloatArray(output);
        }
        catch (NullPointerException e){
            Log.d("TFFusionClassify","Empty recognition input! (NullPointerException)");
            return new float[NUM_CLASSES];
        }


    }

    float[] recognizeAcc(float[] accReadings) {
//        System.out.println("Begin accelerometer inference");
        float[] retval = recognize(accReadings, SHAPE_SENSOR_ACC, ACC_INPUT, ACC_OUTPUT);

        return retval;
    }

    float[] recognizeGyr(float[] gyrReadings) {
        float[] retval = recognize(gyrReadings, SHAPE_SENSOR_GYR, GYR_INPUT, GYR_OUTPUT);

        return retval;
    }

    float[] recognizeMag(float[] magReadings) {
        float[] retval = recognize(magReadings, SHAPE_SENSOR_MAG, MAG_INPUT, MAG_OUTPUT);

        return retval;
    }

    float[] recognizeImg(ArrayList<float[]> frameList) {
        final long[] IMG_SEQUENCE_SHAPE = new long[]{1, NUM_TIME_STEPS_IMG, IMG_FEATURE_SIZE};
        float[] imgFeatFloat = new float[NUM_TIME_STEPS_IMG * IMG_FEATURE_SIZE];

        for (int i = 0; i < NUM_TIME_STEPS_IMG; i++) {
            System.out.print(i + " - ");
            System.arraycopy(frameList.get(i), 0, imgFeatFloat,
                    i * IMG_FEATURE_SIZE, IMG_FEATURE_SIZE);
        }

        float[] retval = recognize(imgFeatFloat, IMG_SEQUENCE_SHAPE, IMG_LSTM_INPUT, IMG_LSTM_OUTPUT);
        return retval;
    }

    float[] recognizeImg(float[] imgFeatFloat) {
        final long[] IMG_SEQUENCE_SHAPE = new long[]{1, NUM_TIME_STEPS_IMG, IMG_FEATURE_SIZE};
        float[] retval = recognize(imgFeatFloat, IMG_SEQUENCE_SHAPE, IMG_LSTM_INPUT, IMG_LSTM_OUTPUT);
        return retval;
    }

    float[] recognizeInception(float[] bitmap) {

        long[] IMAGE_SHAPE = new long[]{1, IMAGE_INPUT_SIZE, IMAGE_INPUT_SIZE, NUM_CHANNELS_IMG};
        try (Tensor inputTensor = Tensor.create(IMAGE_SHAPE, FloatBuffer.wrap(bitmap));
             Tensor output = sess.runner()
                     .feed(INCEPTION_INPUT_NAME, inputTensor)
                     .fetch(INCEPTION_OUTPUT_NAME)
                     .run().get(0)) {
//            System.out.println("End Inception inference");
            return tensorToFloatArray(output);
        } catch (NullPointerException e) {
            return new float[NUM_CLASSES];
        }
    }

    float[] recogFusionAve(float[] img, float[] acc, float[] gyr, float[] mag) {
        final long[] SHAPE_RECOG = new long[]{1, NUM_CLASSES};
        float[] recog = new float[NUM_CLASSES];
        // If all 0, don't include
        try (Tensor imgTensor = Tensor.create(SHAPE_RECOG, FloatBuffer.wrap(img));
             Tensor accTensor = Tensor.create(SHAPE_RECOG, FloatBuffer.wrap(acc));
             Tensor gyrTensor = Tensor.create(SHAPE_RECOG, FloatBuffer.wrap(gyr));
             Tensor magTensor = Tensor.create(SHAPE_RECOG, FloatBuffer.wrap(mag));

             Tensor output = sess.runner()
                     .feed(FUSION_AVE_IMG_INPUT_NAME, imgTensor)
                     .feed(FUSION_AVE_ACC_INPUT_NAME, accTensor)
                     .feed(FUSION_AVE_GYR_INPUT_NAME, gyrTensor)
                     .feed(FUSION_AVE_MAG_INPUT_NAME, magTensor)
                     .fetch(FUSION_AVE_OUTPUT_NAME)
                     .run().get(0)) {
            return tensorToFloatArray(output);


        }

    }

    float[] recogFusion(float[] img, float[] acc, float[] gyr, float[] mag) {
        final long[] SHAPE_IMG = new long[]{1, NUM_TIME_STEPS_IMG, IMG_FEATURE_SIZE};

        // If all 0, don't include
        try (Tensor imgTensor = Tensor.create(SHAPE_IMG, FloatBuffer.wrap(img));
             Tensor accTensor = Tensor.create(SHAPE_SENSOR_ACC, FloatBuffer.wrap(acc));
             Tensor gyrTensor = Tensor.create(SHAPE_SENSOR_GYR, FloatBuffer.wrap(gyr));
             Tensor magTensor = Tensor.create(SHAPE_SENSOR_MAG, FloatBuffer.wrap(mag));

             Tensor output = sess.runner()
                     .feed(IMG_LSTM_INPUT, imgTensor)
                     .feed(ACC_INPUT, accTensor)
                     .feed(GYR_INPUT, gyrTensor)
                     .feed(MAG_INPUT, magTensor)
                     .fetch(FUSION_OUTPUT_NAME)
                     .run().get(0)) {
            return tensorToFloatArray(output);


        } catch (NullPointerException e) {
            return new float[NUM_CLASSES];
        }

    }

    private float[] standardizeSensor(float[][] sensorValues, float[] sensorMean, float[] sensorStd, int NUM_TIME_STEPS_SENSOR) {
        // Format is XYZ-XYZ-XYZ row major
        float[] flat = new float[NUM_CHANNELS_SENSOR * NUM_TIME_STEPS_SENSOR];
        for (int i = 0; i < sensorValues.length; i++) {
            System.arraycopy(sensorValues[i], 0, flat, i * NUM_CHANNELS_SENSOR, NUM_CHANNELS_SENSOR);
            for (int j = 0; j < sensorMean.length; j++) {
                flat[i * NUM_CHANNELS_SENSOR + j] = (flat[i * NUM_CHANNELS_SENSOR + j] - sensorMean[j]) / sensorStd[j];
            }

        }
        return flat;
    }

    float[] standardizeSensor(float[] sensorValues, float[] sensorMean, float[] sensorStd) {
        // Meant to standardize a single timestep
        float[] newValues = new float[sensorValues.length];
        for (int i = 0; i < sensorValues.length; i++) {
            newValues[i] = (Math.abs(sensorValues[i]) - sensorMean[i]) / sensorStd[i];
        }
        return newValues;
    }

    float[] meanOfRecognitions(float[][] recognitions) {
        // tensor shape of recognitions = {?,9}
        final String MEAN_INPUT_NAME = "Mean/mean_input";
        final String MEAN_OUTPUT_NAME = "Mean/mean_output";
        // Flatten
        float[] flat = new float[NUM_CLASSES * recognitions.length];
        for (int i = 0; i < recognitions.length; i++) {
            System.arraycopy(recognitions[i], 0, flat, i * NUM_CLASSES, NUM_CLASSES);
        }
        return recognize(flat, new long[]{recognitions.length, NUM_CLASSES}, MEAN_INPUT_NAME, MEAN_OUTPUT_NAME);
    }

    float[] meanOfRecognitions(ArrayList<float[]> recognitions) {
        // tensor shape of recognitions = {?,NUM_CLASSES}

        // Flatten
        float[] flat = TFFusionClassifier.flattenRecog(recognitions);
        return recognize(flat, new long[]{recognitions.size(), NUM_CLASSES}, MEAN_INPUT_NAME, MEAN_OUTPUT_NAME);
    }

    float[] meanOfRecogIncremental(float[] newRecog, float[] ave, int numRecog) {
        for (int i = 0; i < newRecog.length; i++) {
            ave[i] = (ave[i] * numRecog + newRecog[i]) / (numRecog + 1);
        }
        return ave;
    }

    void close() {
        sess.close();
    }

    boolean allIsNan(float[] test) {
        for (float f : test) {
            if (!Float.isNaN(f)) {
                return false;
            }
        }
        return true;
    }

}

