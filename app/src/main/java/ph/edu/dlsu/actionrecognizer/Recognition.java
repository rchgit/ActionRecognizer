package ph.edu.dlsu.actionrecognizer;

import java.util.*;

/**
 * An immutable result returned by a ClassifierImage describing what was recognized.
 */
public class Recognition {
    /**
     * A unique identifier for what has been recognized. Specific to the class, not the instance of
     * the object.
     */
    private final int recogIndex;

    /**
     * Display name for the recognition.
     */
    private final String title;

    /**
     * A sortable score for how good the recognition is relative to others. Higher should be better.
     */
    private final Float confidence;
    private final long timeStamp;

    /*
     * Optional location within the source image for the location of the recognized object.
     */
//        private RectF location;

    Recognition(
            final int recogIndex, final String title, final Float confidence, final long timeStamp) {
        this.recogIndex = recogIndex;
        this.title = title;
        this.confidence = confidence;
        this.timeStamp = timeStamp;
//            this.location = location;
    }



    public int getRecogIndex() {
        return recogIndex;
    }

    String getTitle() {
        return title;
    }

    Float getConfidence() {
        return confidence;
    }


    @Override
    public String toString() {
        String resultString = "";
        resultString += "[" + timeStamp + "-" + recogIndex + "] ";

        if (title != null) {
            resultString += title + " ";
        }

        if (confidence != null) {
            // Added locale setting to suppress string format locale warnings
            resultString += String.format(Locale.US, "(%.1f%%) ", confidence * 100.0f);
        }

//            if (location != null) {
//                resultString += location + " ";
//            }

        return resultString.trim();
    }

    // Orders the recognitions in descending order.
    static ArrayList<Recognition> sortRecognitions(ArrayList<Recognition> l){
        PriorityQueue<Recognition> pq =
                new PriorityQueue<Recognition>(
                        3,
                        new Comparator<Recognition>() {
                            @Override
                            public int compare(Recognition lhs, Recognition rhs) {
                                // Intentionally reversed to put high confidence at the head of the queue.
                                return Float.compare(rhs.getConfidence(), lhs.getConfidence());
                            }
                        });
        pq.addAll(l);
        return new ArrayList<>(pq);
    }
}
